package com.slalla.ml.randomForest;

import com.slalla.ml.id3.Node;

import java.io.*;

public class Binary {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Node[] randomForest = new Node[100];


        FileInputStream fis = new FileInputStream("RandomForest.txt");
        ObjectInputStream inFile = new ObjectInputStream(fis);

        for (int k = 0; k < 10; k++) {  //writes the binary object file
            randomForest[k] = (Node) inFile.readObject();
        }

        inFile.close();  //closes the binary io file.
    }
}
