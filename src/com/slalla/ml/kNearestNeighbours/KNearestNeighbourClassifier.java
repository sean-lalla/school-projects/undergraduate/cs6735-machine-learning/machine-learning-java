package com.slalla.ml.kNearestNeighbours;

import java.util.HashMap;

public class KNearestNeighbourClassifier {
    private String[][] kNearestNeighbourClassifier;
    private HashMap<Integer, HashMap<String, HashMap<String, Integer>>> classifierTable;
    private HashMap<String, Integer> classValues;

    public KNearestNeighbourClassifier(String[][] kNearestNeighbourClassifier, HashMap<Integer, HashMap<String, HashMap<String, Integer>>> classifierTable, HashMap<String, Integer> classValues) {
        this.kNearestNeighbourClassifier = kNearestNeighbourClassifier;
        this.classifierTable = classifierTable;
        this.classValues = classValues;
    }

    public String[][] getkNearestNeighbourClassifier() {
        return kNearestNeighbourClassifier;
    }

    public void setkNearestNeighbourClassifier(String[][] kNearestNeighbourClassifier) {
        this.kNearestNeighbourClassifier = kNearestNeighbourClassifier;
    }

    public HashMap<Integer, HashMap<String, HashMap<String, Integer>>> getClassifierTable() {
        return classifierTable;
    }

    public void setClassifierTable(HashMap<Integer, HashMap<String, HashMap<String, Integer>>> classifierTable) {
        this.classifierTable = classifierTable;
    }

    public HashMap<String, Integer> getClassValues() {
        return classValues;
    }

    public void setClassValues(HashMap<String, Integer> classValues) {
        this.classValues = classValues;
    }
}
