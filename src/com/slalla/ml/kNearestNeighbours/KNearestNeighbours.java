package com.slalla.ml.kNearestNeighbours;

import com.slalla.ml.dataTransformation.DataHelper;
import com.slalla.ml.dataTransformation.FileHelper;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class KNearestNeighbours {

    public static void main(String args[]){
        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin2.data", "car.data", "ecoli2.data", "letter-recognition.data", "mushroom2.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the K Nearest Neighbours Classifier. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > fileNames.length);

        String fileName = fileNames[input - 1];

        int[] rowCols;
        try {
            rowCols = FileHelper.getNumRowsColumns(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int numRows = rowCols[0];
        int numCols = rowCols[1];


        String[][] inputMatrix;

        try {
            inputMatrix = FileHelper.readInValues(numRows, numCols, fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        int classifierColumn = numCols;
        do {
            System.out.println("Please select the column you wish to predict. There are " + numCols + " columns.");
            classifierColumn = inputScanner.nextInt();
        } while (classifierColumn < 0 || classifierColumn >= numCols);

        int symbolicColumn;
        HashSet<Integer> symbolicColumns = new HashSet<Integer>();
        do {
            System.out.println("Please enter a column to classify as symbolic. There are " + numCols + " columns. -1 to quit");
            symbolicColumn = inputScanner.nextInt();
            if(symbolicColumn >= 0 && symbolicColumn < inputMatrix[0].length && symbolicColumn != classifierColumn){
                symbolicColumns.add(symbolicColumn);
            }
            else if(symbolicColumn ==-1){
                //do nothing
            }
            else{
                System.out.println("That column was invalid.");
            }
        } while (symbolicColumn != -1);

        int numericColumn;
        HashSet<Integer> numericColumns = new HashSet<Integer>();
        do {
            System.out.println("Please enter a column to classify as numeric. There are " + numCols + " columns. -1 to quit");
            numericColumn = inputScanner.nextInt();
            if(numericColumn >= 0 && numericColumn < inputMatrix[0].length && numericColumn != classifierColumn && !symbolicColumns.contains(numericColumn)){
                numericColumns.add(numericColumn);
            }
            else if(numericColumn ==-1){
                //do nothing
            }
            else{
                System.out.println("That column was invalid.");
            }
        } while (numericColumn != -1);



        //Number of times to run k-fold-cross validation
        ArrayList<Double> nTimeAccuracies = new ArrayList<>();
        for (int ntime = 0; ntime < 10; ntime++) {
            //Shuffle
            //inputMatrix = DataHelper.shuffleData(ntime, inputMatrix); used for predictable shuffled data
            inputMatrix = DataHelper.shuffleData(inputMatrix);

            int numberOfFolds = 5;
            ArrayList<String[][]> folds = DataHelper.kfoldSplit(inputMatrix, numberOfFolds);
            ArrayList<Double> kFoldAccuracies = new ArrayList<Double>();
            for (int i = 0; i < folds.size(); i++) {
                HashSet<Integer> foldsToUse = new HashSet<Integer>();
                int fold = i;
                while (foldsToUse.size() != numberOfFolds - 1) {
                    fold = (fold + 1) % (numberOfFolds);
                    foldsToUse.add(fold);
                }
                System.out.println(foldsToUse);
                String[][] combinedFold = DataHelper.combineFolds(folds, foldsToUse);
                String[][] testingFold = folds.get((fold + 1) % (numberOfFolds));

                //Normalizes numericColumns
                KNearestNeighbourClassifier kNearestNeighbourClassifier = normalizeData(combinedFold, numericColumns);

                //Finds probabilities for symbolicColumns
                getProbabilitiesForSymbolicColumns(kNearestNeighbourClassifier, symbolicColumns, classifierColumn);

                int numTestExamples = testingFold.length;
                int correct = classifyMultiple(kNearestNeighbourClassifier, testingFold, classifierColumn, symbolicColumns, numericColumns);
                double percentage = ((double) correct) / numTestExamples;
                kFoldAccuracies.add(percentage);
            }
            nTimeAccuracies.add(DataHelper.getAverage(kFoldAccuracies));
        }
        System.out.println("Average accuracy was " + DataHelper.getAverage(nTimeAccuracies));
        System.out.println("Standard Deviation was " + DataHelper.getStdev(nTimeAccuracies));
    }


    /**
     * Gets the probabilities for symbolic columns storing them in our classifier
     * @param kNearestNeighbourClassifier The KNearestNeighbour classifier we are training
     * @param symbolicColumns The symbolic columns in this dataset
     * @param classifierColumn The classifier column in this dataset
     */
    private static void getProbabilitiesForSymbolicColumns(KNearestNeighbourClassifier kNearestNeighbourClassifier, HashSet<Integer> symbolicColumns, int classifierColumn) {
        //Initializes the occurrences of classifier values to be 0.
        String[][] classifierMatrix =  kNearestNeighbourClassifier.getkNearestNeighbourClassifier();
        HashMap<Integer, HashMap<String, HashMap<String, Integer>>> classifierTable = kNearestNeighbourClassifier.getClassifierTable();
        for(int i: symbolicColumns){
            classifierTable.put(i, new HashMap<String, HashMap<String, Integer>>());
        }

        HashMap<String, Integer> classifierValueOccurences = kNearestNeighbourClassifier.getClassValues();
        for (String[] row : classifierMatrix) {
            String className = row[classifierColumn];
            if (classifierValueOccurences.containsKey(className)) {
                int value = classifierValueOccurences.get(className);
                classifierValueOccurences.put(className, value + 1);
            }
            else {
                classifierValueOccurences.put(className, 0);
            }
        }

        for (int symbolicColumn : symbolicColumns) {
            HashMap<String, HashMap<String, Integer>> currentAttribute = classifierTable.get(symbolicColumn);
            for(int i =0; i< classifierMatrix.length; i++){
                String classOfRow = classifierMatrix[i][classifierColumn];
                String currentAttributeValue = classifierMatrix[i][symbolicColumn];
                if (currentAttribute.containsKey(currentAttributeValue)){
                    HashMap<String, Integer> attributeAndClassCorrect = currentAttribute.get(currentAttributeValue);
                    Integer currentCount = attributeAndClassCorrect.get(classOfRow);
                    attributeAndClassCorrect.put(classOfRow, currentCount+1);
                }
                else{
                    HashMap<String, Integer> classificationForAttributeI = new HashMap<String, Integer>();
                    for(String classifierValues : classifierValueOccurences.keySet().toArray(new String[0])){
                        if(classifierValues.equals(classOfRow)) {
                            classificationForAttributeI.put(classifierValues, 1);
                        }
                        else{
                            classificationForAttributeI.put(classifierValues, 0);
                        }
                    }
                    currentAttribute.put(currentAttributeValue, classificationForAttributeI);
                }
            }
        }
    }

    /**
     * Normalizes the numeric columns in the dataset
     * @param inputMatrix The input matrix we are trying to normalize
     * @param numericColumns The numeric columns in the dataset
     * @return A KNearestNeighbourClassifier that has numeric columns normalized
     */
    private static KNearestNeighbourClassifier normalizeData(String[][] inputMatrix, HashSet<Integer> numericColumns){
       for(Integer numericColumn: numericColumns){

           //Finds the maximum value of the column
           double max = Double.parseDouble(inputMatrix[0][numericColumn]);
           for(int i = 1; i<inputMatrix.length; i++){
               double value = Double.parseDouble(inputMatrix[i][numericColumn]);
               if(value > max){
                   max = value;
               }
           }

           //Divides the current column by its max value to normalize it between 0 and 1.
           for(int i = 0; i<inputMatrix.length; i++){
               double normalizedValue = Double.parseDouble(inputMatrix[i][numericColumn])/max;
               inputMatrix[i][numericColumn] = normalizedValue+"";
           }

       }
       return new KNearestNeighbourClassifier(inputMatrix, new HashMap<Integer, HashMap<String, HashMap<String, Integer>>>(), new HashMap<String, Integer>());
    }

    /**
     * Calculates the distance between e1 and e2.
     * @param e1 The item we wish to classify
     * @param e2 The item to compare e1 to
     * @param symbolicColumns The list of symbolic columns
     * @param numericColumns The list of numeric columns
     * @param classifierColumn The classifier column
     * @param kNearestNeighbourClassifier The KNearestNeighbour classifier we have trained
     * @return The distance between e1 and e2
     */
    private static Neighbour distance(String[] e1, String[] e2, HashSet<Integer> symbolicColumns, HashSet<Integer> numericColumns, int classifierColumn, KNearestNeighbourClassifier kNearestNeighbourClassifier){
        HashMap<Integer, HashMap<String, HashMap<String, Integer>>> classifierTable = kNearestNeighbourClassifier.getClassifierTable();
        double sum = 0;
        for (int i = 0; i< e1.length; i++){
            //If the column we are looking at is symbolic calculate distance based on probability
            if(symbolicColumns.contains(i)){
                for(String classifier: kNearestNeighbourClassifier.getClassValues().keySet()){
                    double probabilityOfCGivenR = 0;
                    if(classifierTable.get(i).containsKey(e1[i])) {
                        probabilityOfCGivenR = classifierTable.get(i).get(e1[i]).get(classifier);
                    }
                    double probabilityOfCGivenS = classifierTable.get(i).get(e2[i]).get(classifier);

                    probabilityOfCGivenR /= kNearestNeighbourClassifier.getClassValues().get(classifier);
                    probabilityOfCGivenS /= kNearestNeighbourClassifier.getClassValues().get(classifier);
                    sum += Math.pow((probabilityOfCGivenR - probabilityOfCGivenS),2);
                }
            }
            //If the column is numeric just calculate the difference between them and square it
            else if(numericColumns.contains(i)){
                sum += Math.pow(Double.parseDouble(e1[i]) - Double.parseDouble(e2[i]),2);
            }
        }
        double distance = Math.sqrt(sum)/(e1.length-1);
        Neighbour neighbour = new Neighbour(distance, e2[classifierColumn]);
        return neighbour;
    }

    /**
     * Calculates the K nearest neighbours to our example we wish to classify
     * @param k The number of closest neighbours we wish to find
     * @param kNearestNeighbourClassifier The kNearestNeighbour classifier we have trained
     * @param exampleToClassify The example we wish to classify
     * @param classifierColumn The column used for classification
     * @param symbolicColumns The list of symbolic columns
     * @param numericColumns The list of numeric columns
     * @return An ArrayList of k Neighbours with their classification and distance
     */
    private static ArrayList<Neighbour> kNearestNeighbours(int k, KNearestNeighbourClassifier kNearestNeighbourClassifier, String[] exampleToClassify, int classifierColumn, HashSet<Integer> symbolicColumns, HashSet<Integer> numericColumns){
        ArrayList<Neighbour> kNearest = new ArrayList<Neighbour>();
        String[][] matrix = kNearestNeighbourClassifier.getkNearestNeighbourClassifier();
        for(int i = 0; i <matrix.length; i++){
            Neighbour n = distance(exampleToClassify, matrix[i], symbolicColumns, numericColumns, classifierColumn, kNearestNeighbourClassifier);

            //Keeps a sorted list of neighbours so we can find the kNearestNeighbours
            if(kNearest.size() ==0){
                kNearest.add(n);
            }
            else if(kNearest.size()<k){
                int count = 0;
                while(count<kNearest.size() && n.getDistance()>kNearest.get(count).getDistance()){
                    count++;
                }
                kNearest.add(count, n);
            }
            else{
                if(n.getDistance() <= kNearest.get(k-1).getDistance()){
                    kNearest.remove(k-1);
                    int count = 0;
                    while(count<kNearest.size() && n.getDistance()>kNearest.get(count).getDistance()){
                        count++;
                    }
                    kNearest.add(count, n);
                }
            }
        }
        return kNearest;
    }

    /**
     * Returns the classification for an exampleToClassify by finding the most common classification on its K nearest neighbours
     * @param kNearestNeighbourClassifier The K nearest neighbour classifier we have trained
     * @param exampleToClassify The example we wish to classify
     * @param classifierColumn The column being used for classifications
     * @param symbolicColumns The symbolic columns in this dataset
     * @param numericColumns The numeric columns in this dataset
     * @return The classification for the exampleToClassify based on its K nearest neighbours
     */
    private static String classify(KNearestNeighbourClassifier kNearestNeighbourClassifier, String[] exampleToClassify, int classifierColumn, HashSet<Integer> symbolicColumns, HashSet<Integer> numericColumns){
        HashMap<String, Integer> countClass = new HashMap<String, Integer>();
        ArrayList<Neighbour> kNearestClasses = kNearestNeighbours(5, kNearestNeighbourClassifier, exampleToClassify, classifierColumn, symbolicColumns, numericColumns);

        for(Neighbour k: kNearestClasses){
            if(countClass.containsKey(k.getClassName())){
                countClass.put(k.getClassName(), countClass.get(k.getClassName()) +1);
            }
            else{
                countClass.put(k.getClassName(), 1);
            }
        }

        //Finds the max value among the keys available
        String[] possibleClassifications = countClass.keySet().toArray(new String[0]);
        String classification = possibleClassifications[0];
        for(int i = 1; i<possibleClassifications.length; i++){
            if(countClass.get(possibleClassifications[i]) > countClass.get(classification)){
                classification = possibleClassifications[i];
            }
        }
        return classification;
    }

    /**
     * Classifies multiple examples and returns the number of correctly classified examples.
     * @param kNearestNeighbourClassifier The k nearest neighbour classified we have trained
     * @param examplesToClassify The examples we wish to classify
     * @param classifierColumn The classifier column in this dataset
     * @param symbolicColumns The symbolic columns in this dataset
     * @param numericColumns The numeric columns in this dataset
     * @return The number of correctly classified instances
     */
    private static int classifyMultiple(KNearestNeighbourClassifier kNearestNeighbourClassifier, String[][] examplesToClassify, int classifierColumn, HashSet<Integer> symbolicColumns, HashSet<Integer> numericColumns) {
        int correct = 0;
        for (int i = 0; i < examplesToClassify.length; i++) {
            String classification = classify(kNearestNeighbourClassifier, examplesToClassify[i], classifierColumn, symbolicColumns, numericColumns);
            if (classification.equals(examplesToClassify[i][classifierColumn])) {
                correct++;
            }
        }
        return correct;
    }

}
