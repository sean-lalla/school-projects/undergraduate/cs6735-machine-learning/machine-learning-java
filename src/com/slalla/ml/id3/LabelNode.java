package com.slalla.ml.id3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class LabelNode extends Node implements Serializable {
    String classLabel;

    public LabelNode(String classLabel) {
        this.classLabel = classLabel;
    }

    public String getClassLabel() {
        return classLabel;
    }

    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

//    public void writeObject(ObjectOutputStream outputStream)throws IOException {
//        outputStream.defaultWriteObject();
//
//    }
//
//    public void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
//        inputStream.defaultReadObject();
//
//    }
}
