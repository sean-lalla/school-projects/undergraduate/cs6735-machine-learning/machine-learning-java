package com.slalla.ml.id3;

public class InformationGain {
    private int attribute;
    private double value;

    public InformationGain(int attribute, double value) {
        this.attribute = attribute;
        this.value = value;
    }

    public int getAttribute() {
        return attribute;
    }

    public void setAttribute(int attribute) {
        this.attribute = attribute;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
