package com.slalla.ml.id3;

import com.slalla.ml.dataTransformation.DataHelper;
import com.slalla.ml.dataTransformation.FileHelper;

import java.io.FileNotFoundException;
import java.util.*;

public class ID3 {

    public static void main(String[] args){
        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin2.data", "car.data", "ecoli2.data", "letter-recognition.data", "mushroom2.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the ID3 Classifier. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > fileNames.length);

        String fileName = fileNames[input - 1];

        int[] rowCols;
        try {
            rowCols = FileHelper.getNumRowsColumns(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int numRows = rowCols[0];
        int numCols = rowCols[1];


        String[][] inputMatrix;

        try {
            inputMatrix = FileHelper.readInValues(numRows, numCols, fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }


        int classifierColumn = numCols;
        do {
            System.out.println("Please select the column you wish to predict. There are " + numCols + " columns.");
            classifierColumn = inputScanner.nextInt();
        } while (classifierColumn < 0 || classifierColumn >= numCols);


        //Number of times to run k-fold-cross validation
        ArrayList<Double> nTimeAccuracies = new ArrayList<>();
        for (int ntime = 0; ntime < 10; ntime++) {
            //Shuffle
            //inputMatrix = DataHelper.shuffleData(ntime, inputMatrix); used for predictable shuffled data
            inputMatrix = DataHelper.shuffleData(inputMatrix);

            int numberOfFolds = 5;
            ArrayList<String[][]> folds = DataHelper.kfoldSplit(inputMatrix, numberOfFolds);
            ArrayList<Double> kFoldAccuracies = new ArrayList<Double>();
            for (int i = 0; i < folds.size(); i++) {
                HashSet<Integer> foldsToUse = new HashSet<Integer>();
                int fold = i;
                while (foldsToUse.size() != numberOfFolds - 1) {
                    fold = (fold + 1) % (numberOfFolds);
                    foldsToUse.add(fold);
                }
                System.out.println(foldsToUse);
                String[][] combinedFold = DataHelper.combineFolds(folds, foldsToUse);
                String[][] testingFold = folds.get((fold + 1) % (numberOfFolds));


                HashSet<Integer> attributes = new HashSet<Integer>();
                for(int j = 0; j< numCols; j++){
                    if(j != classifierColumn){
                        attributes.add(j);
                    }
                }

                ArrayList<String[]> inputMatrixConvert = new ArrayList<String[]>(Arrays.asList(combinedFold));
                ArrayList<HashSet<String>> uniqueAttributeValues = preTrainID3(inputMatrixConvert, classifierColumn);
                Node decisionTree = trainID3Classifer(inputMatrixConvert, classifierColumn, attributes, uniqueAttributeValues);

                int numTestExamples = testingFold.length;

                int correct = classifyMultiple(decisionTree, testingFold, classifierColumn);
                double percentage = ((double) correct) / numTestExamples;
                kFoldAccuracies.add(percentage);
            }
            nTimeAccuracies.add(DataHelper.getAverage(kFoldAccuracies));
        }
        System.out.println("Average accuracy was " + DataHelper.getAverage(nTimeAccuracies));
        System.out.println("Standard Deviation was " + DataHelper.getStdev(nTimeAccuracies));
    }

    /**
     * Calculates the entropy for a dataset
     * @param S The dataset to calculate entropy for
     * @param classifierValues The possible classifications for examples in the dataset
     * @return The entropy of S
     */
    public static double getEntropy(ArrayList<String[]> S, HashMap<String, Integer> classifierValues){
        double sum = 0;
        for(String classification : classifierValues.keySet() ){
            Integer value = classifierValues.get(classification);
            double p = ((double) value)/S.size();
            double log2p = Math.log(p)/Math.log(2);
            sum += -p * log2p;
        }
        return sum;
    }

    /**
     * Calculates the InformationGain for Attribute A
     * @param S The dataset to calculate entropy for
     * @param A The attribute being used for calculating Information Gain
     * @param classifierValues The possible classifications for examples in the dataset
     * @param attributeVals The possible values for a particular attribute
     * @param classifierColumn The column to be classified
     * @return The information gain by using attribute A
     */
    public static double getInformationGain(ArrayList<String[]> S, int A, HashMap<String, Integer> classifierValues, HashSet<String> attributeVals, int classifierColumn){
        double gain = getEntropy(S, classifierValues);
        for(String v : attributeVals){
            ArrayList<String[]> Sv = filterSWhereAEqualsV(S, A, v);
            double ratio = ((double)Sv.size())/S.size();

            HashMap<String, Integer> SvClassiferOccurrences = new HashMap<String, Integer>();
            for (int i = 0; i<Sv.size(); i++){
                String classname = Sv.get(i)[classifierColumn];
                if(!SvClassiferOccurrences.containsKey(classname)){
                    SvClassiferOccurrences.put(classname, 1);
                }
                else{
                    int classnameValue = SvClassiferOccurrences.get(classname);
                    SvClassiferOccurrences.put(classname, classnameValue+1);
                }
            }
            gain -= ratio*getEntropy(Sv,SvClassiferOccurrences);
        }

        return gain;
    }

    /**
     * Filters the dataset s where Attribute a has value v
     * @param s The dataset to filter
     * @param a The attribute selected
     * @param v The value to filter on
     * @return The subset of the dataset
     */
    public static ArrayList<String[]> filterSWhereAEqualsV(ArrayList<String[]> s, int a, String v) {
        ArrayList<String[]> Sv = new ArrayList<String[]>();
        for (String[] strings : s) {
            if (strings[a].equals(v)) {
                Sv.add(strings);
            }
        }
        return Sv;
    }

    /**
     * Trains a decision tree classifier using the ID3 algorithm
     * @param examples The examples used for training.
     * @param classifierColumn The column to be classified
     * @param attributes The list of attributes that can be used
     * @param uniqueAttributeValues The unique attribute values for each attribute
     * @return An ID3 decision tree classifier
     */
    public static Node trainID3Classifer(ArrayList<String[]> examples, int classifierColumn, HashSet<Integer> attributes, ArrayList<HashSet<String>> uniqueAttributeValues) {
        Node root;

        HashMap<String, Integer> classifierValueOccurences = new HashMap<String, Integer>();
        for (int i = 0; i<examples.size(); i++){
            String classname = examples.get(i)[classifierColumn];
            if(!classifierValueOccurences.containsKey(classname)){
                classifierValueOccurences.put(classname, 1);
            }
            else{
                int classnameValue = classifierValueOccurences.get(classname);
                classifierValueOccurences.put(classname, classnameValue+1);
            }
        }

        //Gets all class labels and sets the initial max to the first class for
        //the case where attributes is empty.
        HashMap<String, Integer> maxClass = new HashMap<String, Integer>();
        String max ="";
        double threshold = 0.95;

        //Figures out if we are at a leaf node because all values are correctly classified
        for(String classname : classifierValueOccurences.keySet()){
            Integer value  = classifierValueOccurences.get(classname);
            if(value>= threshold*examples.size()){
                return new LabelNode(classname);
            }
            else if (maxClass.isEmpty()){
                maxClass.put(classname, classifierValueOccurences.get(classname));
                max = classname;
            }
            else{
                if(maxClass.get(max) < value){
                    max = classname;
                    maxClass.clear();
                    maxClass.put(classname, value);
                }
            }
        }

        if(attributes.size()==0){
            return new LabelNode(maxClass.keySet().toArray(new String[0])[0]);
        }
        else {
            InformationGain maxGain = null;
            for (int attribute : attributes) {
                double gain = getInformationGain(examples, attribute, classifierValueOccurences, uniqueAttributeValues.get(attribute), classifierColumn);
                if (maxGain == null) {
                    maxGain = new InformationGain(attribute, gain);
                }
                else if (maxGain.getValue() < gain) {
                    maxGain = new InformationGain(attribute, gain);
                }
            }
            //A <- Attribute that best classifies examples
            root = new AttributeNode(maxGain.getAttribute());
            //For each possible value of vi of A
            for(String vi : uniqueAttributeValues.get(maxGain.getAttribute())){
                Node node = new ValueNode(vi);
                root.getChildren().add(node);
                //Let Examples(vi) be the subset of examples that have the value vi for A
                ArrayList<String[]> examplesVi = filterSWhereAEqualsV(examples, maxGain.getAttribute(), vi);
                //If Examples(vi) is empty
                //Then below this new branch create a leaf node with label = most common target value in the examples
                if(examplesVi.isEmpty()){
                    node.getChildren().add(new LabelNode(maxClass.keySet().toArray(new String[0])[0]));
                }
                else{
                    //below this new branch add the subtree ID3(Examples(vi), Target_Attribute, Attributes -{A})
                    HashSet<Integer> attributesMinusA = new HashSet<Integer>(attributes);
                    attributesMinusA.remove(maxGain.getAttribute());
                    Node child = trainID3Classifer(examplesVi, classifierColumn, attributesMinusA, uniqueAttributeValues);
                    node.getChildren().add(child);
                }
            }
        }
        return root;
    }

    /**
     * Finds the unique attribute values for every attribute
     * @param examples The examples to use for getting values
     * @param classifierColumn The classifier column
     * @return The unique attribute values for every attribute
     */
    public static ArrayList<HashSet<String>> preTrainID3(ArrayList<String[]> examples, int classifierColumn){
        ArrayList<HashSet<String>> uniqueAttributeVals = new ArrayList<HashSet<String>>();
        for(int j = 0; j< examples.get(0).length; j++){
            if(j == classifierColumn){
                uniqueAttributeVals.add(null);
            }
            else{
                HashSet <String> attributeVals = new HashSet<String>();
                for (String[] example : examples) {
                    attributeVals.add(example[j]);
                }
                uniqueAttributeVals.add(attributeVals);
            }
        }
        return uniqueAttributeVals;
    }

    /**
     * Classifies an example based on the decision tree
     * @param decisionTree The model being used for classification
     * @param exampleToClassify The example to be classified
     * @return The predicted class for the example
     */
    public static String classify(Node decisionTree, String[] exampleToClassify) {
        if(decisionTree instanceof LabelNode){
            return ((LabelNode)(decisionTree)).getClassLabel();
        }
        else{
            AttributeNode attributeNode =((AttributeNode)(decisionTree));
            int attribute = attributeNode.getColumn();
            ArrayList<Node> children = attributeNode.getChildren();
            Node selectRoute = null;
            boolean found = false;
            for(int i = 0; i < children.size() && !found; i++){
                ValueNode route = ((ValueNode)(children.get(i)));
                if(route.getValue().equals(exampleToClassify[attribute])){
                    selectRoute = route;
                    found = true;
                }
            }
            //It may be possible that selectRoute is null because the trained tree doesn't know what to do with
            //a value for that attribute which was not seen in the training data.
            if(selectRoute == null){
                return "";
            }
            Node subTree = selectRoute.getChildren().get(0);
            return classify(subTree, exampleToClassify);
        }
    }

    /**
     * Classifies multiple examples and returns the number of correctly classified examples.
     * @param decisionTree The decision tree learned
     * @param examplesToClassify The examples we wish to classify
     * @param classifierColumn The classifier column in this dataset
     * @return The number of correctly classified instances
     */
    public static int classifyMultiple(Node decisionTree, String[][] examplesToClassify, int classifierColumn ){
        int correct = 0;
        for (int i = 0; i < examplesToClassify.length; i++) {
            String classification = classify(decisionTree, examplesToClassify[i]);
            if (classification.equals(examplesToClassify[i][classifierColumn])) {
                correct++;
            }
        }
        return correct;
    }

}
