package com.slalla.ml.bayes;

import java.util.ArrayList;
import java.util.HashMap;

public class BayesClassifier {
    private HashMap<String, Integer> classifierValueOccurences;
    private ArrayList<HashMap<String, HashMap<String, Integer>>> bayesClassifierTable;
    private int classifierColumn;

    public BayesClassifier(HashMap<String, Integer> classifierValueOccurences, ArrayList<HashMap<String, HashMap<String, Integer>>> bayesClassifierTable, int classifierColumn) {
        this.classifierValueOccurences = classifierValueOccurences;
        this.bayesClassifierTable = bayesClassifierTable;
        this.classifierColumn = classifierColumn;
    }

    public HashMap<String, Integer> getClassifierValueOccurences() {
        return classifierValueOccurences;
    }

    public void setClassifierValueOccurences(HashMap<String, Integer> classifierValueOccurences) {
        this.classifierValueOccurences = classifierValueOccurences;
    }

    public ArrayList<HashMap<String, HashMap<String, Integer>>> getBayesClassifierTable() {
        return bayesClassifierTable;
    }

    public void setBayesClassifierTable(ArrayList<HashMap<String, HashMap<String, Integer>>> bayesClassifierTable) {
        this.bayesClassifierTable = bayesClassifierTable;
    }

    public int getClassifierColumn() {
        return classifierColumn;
    }

    public void setClassifierColumn(int classifierColumn) {
        this.classifierColumn = classifierColumn;
    }
}
