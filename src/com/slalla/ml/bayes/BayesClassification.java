package com.slalla.ml.bayes;

public class BayesClassification {
    private String classification;
    private double percentage;

    public BayesClassification(String classification, double percentage) {
        this.classification = classification;
        this.percentage = percentage;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
}
