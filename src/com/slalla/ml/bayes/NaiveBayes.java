package com.slalla.ml.bayes;

import com.slalla.ml.dataTransformation.DataHelper;
import com.slalla.ml.dataTransformation.FileHelper;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class NaiveBayes {
    public static void main(String[] args) {
        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin2.data", "car.data", "ecoli2.data", "letter-recognition.data", "mushroom2.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the Naive Bayes Classifier. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > 5);

        String fileName = fileNames[input - 1];

        int[] rowCols;
        try {
            rowCols = FileHelper.getNumRowsColumns(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int numRows = rowCols[0];
        int numCols = rowCols[1];


        String[][] inputMatrix;

        try {
            inputMatrix = FileHelper.readInValues(numRows, numCols, fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        int classifierColumn = numCols;
        do {
            System.out.println("Please select the column you wish to predict. There are " + numCols + " columns.");
            classifierColumn = inputScanner.nextInt();
        } while (classifierColumn < 0 || classifierColumn >= numCols);


        //Number of times to run k-fold-cross validation
        ArrayList<Double> nTimeAccuracies = new ArrayList<>();
        for (int ntime = 0; ntime < 10; ntime++) {
            //Shuffle
            //inputMatrix = DataHelper.shuffleData(ntime, inputMatrix); used for predictable shuffled data
            inputMatrix = DataHelper.shuffleData(inputMatrix);

            int numberOfFolds = 5;
            ArrayList<String[][]> folds = DataHelper.kfoldSplit(inputMatrix, numberOfFolds);
            ArrayList<Double> kFoldAccuracies = new ArrayList<Double>();
            for (int i = 0; i < folds.size(); i++) {
                HashSet<Integer> foldsToUse = new HashSet<Integer>();
                int fold = i;
                while (foldsToUse.size() != numberOfFolds - 1) {
                    fold = (fold + 1) % (numberOfFolds);
                    foldsToUse.add(fold);
                }
                System.out.println(foldsToUse);
                String[][] combinedFold = DataHelper.combineFolds(folds, foldsToUse);
                String[][] testingFold = folds.get((fold + 1) % (numberOfFolds));

                ArrayList<HashSet<String>> uniqueAttributeValues = getUniqueAttributeValues(combinedFold);
                BayesClassifier classifier = trainBayesClassifier(combinedFold, classifierColumn, uniqueAttributeValues);

                int numTestExamples = testingFold.length;

                int correct = classifyMultiple(classifier, testingFold, classifierColumn);
                double percentage = ((double) correct) / numTestExamples;
                kFoldAccuracies.add(percentage);
            }
            nTimeAccuracies.add(DataHelper.getAverage(kFoldAccuracies));
        }
        System.out.println("Average accuracy was " + DataHelper.getAverage(nTimeAccuracies));
        System.out.println("Standard Deviation was " + DataHelper.getStdev(nTimeAccuracies));
    }

    /**
     * Finds the unique values of each attribute to do training for.
     * @param inputMatrix The training dataset to be used in the model's training
     * @return An ArrayList of HashSets containing the unique values for each column
     */
    private static ArrayList<HashSet<String>> getUniqueAttributeValues(String[][] inputMatrix) {
        ArrayList<HashSet<String>> uniqueAttributeValues = new ArrayList<HashSet<String>>();

        //The unique attributes for each column
        for (int j = 0; j < inputMatrix[0].length; j++) {
            uniqueAttributeValues.add(new HashSet<String>());
        }

        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix[0].length; j++) {
                uniqueAttributeValues.get(j).add(inputMatrix[i][j]);
            }
        }

        return uniqueAttributeValues;
    }

    /**
     * Trains a Bayes classifier by creating a matrix that can be used for predictions.
     * @param inputMatrix           The training dataset to be used in the model's training
     * @param classifierColumn      The column we wish to predict
     * @param uniqueAttributeValues The unique attributes in this Dataset
     * @return A trained Bayes Classifier
     */
    private static BayesClassifier trainBayesClassifier(String[][] inputMatrix, int classifierColumn, ArrayList<HashSet<String>> uniqueAttributeValues) {
        //Initializes the occurrences of classifier values to be 0.
        HashMap<String, Integer> classifierValueOccurences = new HashMap<String, Integer>();
        for (String className : uniqueAttributeValues.get(classifierColumn)) {
            classifierValueOccurences.put(className, 0);
        }

        //The classifier table used by NaiveBayes for classification.
        //An arrayList of HashMaps of HashMaps allows us to mimic the functionality of an ArrayList of 2D arrays
        //which is what is typically used for Bayes
        ArrayList<HashMap<String, HashMap<String, Integer>>> bayesClassifierTable = new ArrayList<HashMap<String, HashMap<String, Integer>>>();
        int tempPass = 0;
        for (int j = 0; j < inputMatrix[0].length; j++) {
            if (j != classifierColumn) {
                bayesClassifierTable.add(new HashMap<String, HashMap<String, Integer>>());
                for (String uniqueAttribute : uniqueAttributeValues.get(j)) {
                    HashMap<String, Integer> count = new HashMap<String, Integer>();
                    for (String className : uniqueAttributeValues.get(classifierColumn)) {
                        count.put(className, 0);
                    }
                    bayesClassifierTable.get(j-tempPass).put(uniqueAttribute, count);
                }
            }
            else{
                tempPass = 1;
            }
        }


        for (int i = 0; i < inputMatrix.length; i++) {
            //Need a way to know if we have passed the classfier column, since we do not store an array for it in the
            //Array List
            int passedClassifierColumn = 0;

            //Keep track of the current class for our hash table
            String currentClass = inputMatrix[i][classifierColumn];
            Integer currentClassifierCount = classifierValueOccurences.get(currentClass);
            classifierValueOccurences.put(currentClass, currentClassifierCount + 1);

            for (int j = 0; j < inputMatrix[0].length; j++) {
                if (j == classifierColumn) {
                    passedClassifierColumn = 1;
                    continue;
                }
                String currentCell = inputMatrix[i][j];
                Integer currentCount = bayesClassifierTable.get(j - passedClassifierColumn).get(currentCell).get(currentClass);
                bayesClassifierTable.get(j - passedClassifierColumn).get(currentCell).put(currentClass, currentCount + 1);
            }
        }

        BayesClassifier classifier = new BayesClassifier(classifierValueOccurences, bayesClassifierTable, classifierColumn);
        return classifier;
    }

    /**
     * Classifies a single example based on the row of information provided to the classifier.
     * @param classifier
     * @param exampleToClassify The example you wish to classify
     * @param classifierColumn  The column containing the class of the example (this is ignored when classifying)
     * @return The class precicted by the Bayes classifier
     */
    private static String classify(BayesClassifier classifier, String[] exampleToClassify, int classifierColumn) {
        HashMap<String, Integer> classifierValueOccurrences = classifier.getClassifierValueOccurences();
        ArrayList<HashMap<String, HashMap<String, Integer>>> bayesClassifierTable = classifier.getBayesClassifierTable();

        BayesClassification classification = null;
        String[] classiferClasses = classifierValueOccurrences.keySet().toArray(new String[0]);
        for (String classifierClass : classiferClasses) {

            //Initial classification odds
            double classificationPercentage = 1;

            //The count of number of times the classifier class occurred
            double countClass = (double) classifierValueOccurrences.get(classifierClass);

            //Performing the product of probabilities
            int passedClassifierColumn = 0;
            for (int j = 0; j < exampleToClassify.length; j++) {
                if (j == classifierColumn) {
                    passedClassifierColumn = 1;
                    continue;
                }

                //LaPlace Smoothing
                String attributeJ = exampleToClassify[j];
                double countAttributeJ;
                int sizeOfAi = bayesClassifierTable.get(j-passedClassifierColumn).size();
                if(bayesClassifierTable.get(j-passedClassifierColumn).containsKey(attributeJ)) {
                    countAttributeJ = (double) bayesClassifierTable.get(j - passedClassifierColumn).get(attributeJ).get(classifierClass);
                }
                else{
                    countAttributeJ = 0;
                }
                double probabilityaiGivenvj = (countAttributeJ +1)/(countClass+sizeOfAi);
                classificationPercentage *= probabilityaiGivenvj;
            }
            if (classification == null) {
                classification = new BayesClassification(classifierClass, classificationPercentage);
            }
            else if (classificationPercentage > classification.getPercentage()) {
                classification = new BayesClassification(classifierClass, classificationPercentage);
            }
        }
        return classification.getClassification();
    }

    /**
     * Returns correct number of elements classified
     * @param classifier
     * @param examplesToClassify
     * @param classifierColumn
     * @return
     */
    private static int classifyMultiple(BayesClassifier classifier, String[][] examplesToClassify, int classifierColumn) {
        int correct = 0;
        for (int i = 0; i < examplesToClassify.length; i++) {
            String classification = classify(classifier, examplesToClassify[i], classifierColumn);
            if (classification.equals(examplesToClassify[i][classifierColumn])) {
                correct++;
            }
        }
        return correct;
    }

}
